#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0

import os
import logging
import argparse
import gitlab
import time
import sys

from typing import Union
from prometheus_client import start_http_server, Gauge
from prometheus_client.core import REGISTRY
from gitlab.exceptions import GitlabGetError, GitlabListError, GitlabHttpError, GitlabAuthenticationError
from requests.exceptions import ConnectionError

# TODO: make this a parameter because ID is different on test instance
TRUSTED_PROJECT_ID = '339'

class GitLabExporter:
    """exports some custom GitLab Prometheus metrics
    """
    def __init__(self, args: argparse.Namespace):
        logging.basicConfig(level=args.log_level.upper())

        self.gitlab_host = args.host
        self.listening_port = args.port
        self.polling_interval_seconds = args.interval

        self.client: gitlab.Gitlab = gitlab.Gitlab(
            f"https://{self.gitlab_host}", private_token=self.gitlab_token()
        )

        self.gitlab_groject_size_metric = Gauge('gitlab_project_size', 'Total size of GitLab project', ['project_id'])
        self.gitlab_projects_metric = Gauge('gitlab_projects', 'Total number of GitLab project', ['instance'])
        self.gitlab_users_metric = Gauge('gitlab_users', 'Total number of GitLab users', ['instance'])
        self.gitlab_runners_metric = Gauge('gitlab_runners', 'Total number of GitLab runners', ['instance'])
        # TODO: clear() method is added in 0.10.0, remove this workaround when on bookworm
        #self.gitlab_runners_up = Gauge('gitlab_runners_up', 'Status of gitlab-runner', ['instance', 'id', 'description', 'tag_list', 'access_level', 'locked', 'runner_type'])

    def run_metrics_loop(self):
        """Metrics fetching loop from GitLab API"""

        while True:
            self.fetch()
            time.sleep(int(self.polling_interval_seconds))

    def fetch(self):
        """
        Get metrics from GitLab and refresh Prometheus metrics with
        new values.
        """

        # try access GitLab API
        try:
            # TODO use proper api endpoint in v4.x https://python-gitlab.readthedocs.io/en/v4.5.0/gl_objects/statistics.html
            statistics = self.client.http_get("/application/statistics")
            # TODO: old python3-gitlab package does not support runners.all(), remove when on bookworm and when on v4.x
            # So get all runners for the tursted project:
            trusted_project = self.client.projects.get(TRUSTED_PROJECT_ID)
            runners = trusted_project.runners.list()
        except (GitlabListError, GitlabHttpError, ConnectionError) as e:
            logging.error(f"Failed to connect to GitLab API, retrying in {self.polling_interval_seconds} seconds. {e}")
            return
        except GitlabAuthenticationError as e:
            logging.error(f"Insufficient permissions, use a token with API and Admin scope and the correct --host flag: {e}")
            sys.exit(1)


        # Update Prometheus project metrics
        self.gitlab_projects_metric.labels(self.gitlab_host).set(int(statistics["projects"].replace(",","")))

        # Update Prometheus user metrics (quite expensive)
        self.gitlab_users_metric.labels(self.gitlab_host).set(int(statistics["users"].replace(",","")))

        # Update Prometheus runner metrics
        self.gitlab_runners_metric.labels(self.gitlab_host).set(len(runners))

        # TODO: clear() method is added in 0.10.0, remove this workaround when on bookworm
        #self.gitlab_runners_up.clear()
        #self.gitlab_runners_up.remove('instance', 'id', 'description', 'tag_list', 'access_level', 'locked', 'runner_type')
        try:
            REGISTRY.unregister(self.gitlab_runners_up)
        except AttributeError:
            logging.error("Unregistering of gitlab_runners_up metric failed")

        self.gitlab_runners_up = Gauge('gitlab_runners_up', 'Status of gitlab-runner', ['instance', 'id', 'description', 'tag_list', 'access_level', 'locked', 'runner_type'])
        for runner in runners:
            try:
                runner_detail = self.client.runners.get(runner.id)
                self.gitlab_runners_up.labels(instance = self.gitlab_host,
                                            id=runner_detail.id,
                                            description=runner_detail.description,
                                            tag_list= runner_detail.tag_list,
                                            access_level=runner_detail.access_level,
                                            locked = runner_detail.locked,
                                            runner_type=runner_detail.runner_type).set(1)
            except GitlabGetError as e:
                logging.error(f"Failed to get runner details from GitLab API, retrying in {self.polling_interval_seconds} seconds. {e}")
                return


    def gitlab_token(self):
        """Gets the gitlab token from either the environment variable, or the secrets file
        in /etc/gitlab-exporter-auth.
        """
        env_token = os.environ.get("GITLAB_TOKEN")
        if env_token:
            return env_token

        try:
            with open("/etc/gitlab-exporter-auth", encoding="utf-8") as secrets_file:
                return secrets_file.read().strip()
        except FileNotFoundError:
            logging.exception(
                "Couldn't find GITLAB_TOKEN env variable set, or /etc/gitlab-exporter-auth for token"
            )

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Gitlab custom prometheus exporter", description="Makes custom set of GitLab metrics available"
    )
    parser.add_argument(
        "--host",
        default="gitlab.devtools.wmcloud.org",
        help="Gitlab host",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="warning",
        help="Log level (debug, info, warning, error, critical)",
    )
    parser.add_argument(
        "-i",
        "--interval",
        default=60,
        help="Polling interval in seconds",
    )
    parser.add_argument(
        "-p",
        "--port",
        default=9169,
        help="Listening port",
    )

    args = parser.parse_args()
    g = GitLabExporter(args)

    # Start up the server to expose the metrics.
    start_http_server(int(args.port))
    # Generate some requests.
    g.run_metrics_loop()
