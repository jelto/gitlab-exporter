# gitlab-exporter

This project contains a small custom python exporter which exposes custom metrics which are not available in the [official GitLab exporter](https://gitlab.com/gitlab-org/ruby/gems/gitlab-exporter). The metrics have wmf-specific use cases and the original exporter is written in Ruby.

## Usage

The exporter has the following parameters:

```
./exporter.py --help
usage: Gitlab custom prometheus exporter [-h] [--host HOST] [-l LOG_LEVEL] [-i INTERVAL] [-p PORT]

Makes custom set of GitLab metrics available

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Gitlab host
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        Log level (debug, info, warning, error, critical)
  -i INTERVAL, --interval INTERVAL
                        Polling interval in seconds
  -p PORT, --port PORT  Listening port
  ```

Additionally the exporter needs a GitLab token with `read_api` and `read_repository` access. The token has to be exported as a environment variable `GITLAB_TOKEN` or as a file in `/etc/gitlab-exporter-auth`.

## Metrics

The exporter supports the following metrics currently:

```
# HELP gitlab_projects Total number of GitLab project
# TYPE gitlab_projects gauge
gitlab_projects{instance="gitlab.devtools.wmcloud.org"} 89.0
# HELP gitlab_users Total number of GitLab users
# TYPE gitlab_users gauge
gitlab_users{instance="gitlab.devtools.wmcloud.org"} 579.0
# HELP gitlab_runners Total number of GitLab runners
# TYPE gitlab_runners gauge
gitlab_runners{instance="gitlab.devtools.wmcloud.org"} 3.0
# HELP gitlab_runners_up Status of gitlab-runner
# TYPE gitlab_runners_up gauge
gitlab_runners_up{access_level="not_protected",description="Shared Runners, running in Wikimedia Cloud Services",id="76",instance="gitlab.devtools.wmcloud.org",locked="False",runner_type="group_type",tag_list="['wmcs']"} 1.0
gitlab_runners_up{access_level="ref_protected",description="Trusted Runners",id="77",instance="gitlab.devtools.wmcloud.org",locked="True",runner_type="project_type",tag_list="['trusted']"} 1.0
gitlab_runners_up{access_level="ref_protected",description="Trusted Dockerfile Runners",id="79",instance="gitlab.devtools.wmcloud.org",locked="True",runner_type="project_type",tag_list="['trusted', 'dockerfile']"} 1.0
```